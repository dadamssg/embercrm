App.Router.map(function() {
    this.resource('companies', { path: '/companies' });

    this.resource('people', function() {

    	this.route('show', { path: '/:person_id'});
    	this.route('edit', { path: '/:person_id/edit'});
    });
});

App.CompaniesRoute = Ember.Route.extend({

  model: function () {
    return this.store.find('company');
  }
});

App.PeopleRoute = Ember.Route.extend({

	model: function () {
		return this.store.find('person');
	}
});



