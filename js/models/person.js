App.Person = DS.Model.extend({
  name: DS.attr('string'),
  city: DS.attr('string')
});

App.Person.FIXTURES = [
 {
   id: 1,
   name: 'David Adams',
   city: 'Euless'
 },
 {
   id: 2,
   name: 'Kevin Bull',
   city: 'Dallas'
 },
 {
   id: 3,
   name: 'Chris Cowen',
   city: 'Corinth'
 }
];