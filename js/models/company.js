App.Company = DS.Model.extend({
  name: DS.attr('string'),
  isCompleted: DS.attr('boolean')
});

App.Company.FIXTURES = [
 {
   id: 1,
   name: 'Adams Inc',
   isCompleted: true
 },
 {
   id: 2,
   name: 'Bull Industries',
   isCompleted: false
 },
 {
   id: 3,
   name: 'Cowen Corp.',
   isCompleted: false
 }
];