<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Ember Starter Kit</title>
  <link rel="stylesheet" href="../../css/normalize.css">
  <link rel="stylesheet" href="../../css/style.css">
</head>
<body>

  <script type="text/x-handlebars">
    <h2>Welcome to Ember.js</h2>

    <aside>
    
    <ul>
        <li><h2>Entities</h2></li>
            <li>
                {{#link-to 'companies'}}
                    Companies
                {{/link-to}}
            </li>

            <li>
                {{#link-to 'people'}}
                    People
                {{/link-to}}
            </li>
    </ul>
    </aside>

    <section class="main">
        {{outlet}}
    </section>
  </script>

  <script type="text/x-handlebars" data-template-name="companies">
    <ul id="company-list">
      {{#each}}
        <li>
          <input type="checkbox" class="toggle">
          <label>{{name}}</label> <button class="destroy"> x </button>
        </li>
      {{/each}}
    </ul>

    <section class="secondary">
        {{outlet}}
    </section>
  </script>

    <script type="text/x-handlebars" data-template-name="people">
    <ul id="people-list">
      {{#each}}
        <li>
          <input type="checkbox" class="toggle">
          {{#link-to 'people.show' this}}
              <label>{{name}}</label> 
          {{/link-to}}
        </li>
      {{/each}}
    </ul>
    <section class="secondary">
      {{outlet}}
    </section>
  </script>

  <script type="text/x-handlebars" data-template-name="people/show">
    <div>
      {{name}} <br/><br/>
      {{city}} <br/><br/>

      {{#link-to 'people.edit' this}}Edit{{/link-to}}
    </div>
  </script>

  <script type="text/x-handlebars" data-template-name="people/edit">
    <div>
      Edit: {{name}} <br/><br/>
      Edit: {{city}}
    </div>
  </script>

  <script src="../../js/libs/jquery-1.9.1.js"></script>
  <script src="../../js/libs/handlebars-1.0.0.js"></script>
  <script src="../../js/libs/ember-1.0.0.js"></script>
  <script src="../../js/libs/ember-data-v1-beta3.js"></script>
  <script src="../../js/app.js"></script>
  <script src="../../js/router.js"></script>
  <script src="../../js/models/company.js"></script>
  <script src="../../js/models/person.js"></script>

</body>
</html>
